package com.zephr.interview

import java.util.concurrent.CompletableFuture

interface Shape {
  fun calculateArea(): CompletableFuture<Double>
}
